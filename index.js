// [SECTION] Creating a simple server using ExpressJS Framework

// Use the require() to load the express module/package
// It allows us to access the methods and functions that will helo us create a server

const express = require("express")

// Create an application using express
// this creates an express application and stores this is a constant called app
// In simple terms, app is now our server

const app = express();

const port = 3000;

// Setup for allowing the server to handle data from requests
// In simple terms, it allows your app to read json data
// Methods used are call middlewares
// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
// API management is one of the common application of middlewares.

app.use(express.json());

// Allows your application to read data from forms
// By default, information received from the url can only be received as a string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Creating Routes

// GET Method
app.get("/", (req, res) => {
	res.send("Hello World")
})

// POST Method

app.post("/", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

// Mock Database

let users = [];

// Signup Method

app.post("/signup", (req, res) => {
	if(req.body.username !== "" && req.body.password !== "") {

		users.push(req.body);
		res.send(`User ${req.body.username} is susccessfully registered!`)
	} else {
		res.send(`Please input BOTH username and password!`)
	}

	console.log(req.body);

})

// Change Password

app.put("/change-password", (req, res) => {
	let message;

	for(let user = 0; user < users.length; user++) {

		if(req.body.username == users[user].username) {

			users[user].password = req.body.password;

			message = `User ${req.body.username} has susccessfully changed their password`;

			break;
		} else {
			message = `User ${req.body.username} does not exist`;
		}
	}

	res.send(message);
	console.log(users);
})


// Objective 1

app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
})

app.get("/users", (req, res) => {
	res.send(users)
})

app.listen(port, () => console.log(`Server is listening at localhost: ${port}`))